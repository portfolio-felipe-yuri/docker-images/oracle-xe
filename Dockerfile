FROM leandro4j/skdev-oracle:19.12.0-ee-starter

ENTRYPOINT [ "docker exec -u 0 -it --workdir /home skdev-oracle bash" ]

COPY ./installers/* /home

RUN unzip -d /home *.zip
RUN tar -xzvf *.gz -C /home/
RUN tar -xvf *.xz -C /home/

RUN export PATH=$PATH:/home/gradle-7.5.1/bin
RUN export PATH=$PATH:/home/jdk-11.0.16.1/bin
RUN export PATH=$PATH:/home/node-v18.12.0-linux-x64//bin

RUN export JAVA_HOME=/home/jdk-11.0.16.1/

RUN yum update -y

RUN yum install curl -y
RUN yum install jq -y
RUN yum install coreutils -y
RUN yum install zip -y

RUN curl https://rclone.org/install.sh --output install.sh

RUN bash install.sh

RUN CREATE DIRECTORY DUMP_DIR AS '/opt/oracle/oradata/dump';

RUN GRANT READ, WRITE ON DIRECTORY DUMP_DIR TO SYSTEM;

RUN exit

RUN impdp system/admin@localhost:1521/xepdb1 full=Y EXCLUDE=SCHEMA:\"LIKE \'APEX_%\'\",SCHEMA:\"LIKE \'FLOWS_%\'\" directory=DMP_DIR dumpfile=sql.dmp logfile=log.log



EXPOSE 1521

